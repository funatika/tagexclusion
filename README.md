# README - TagExclusion #

Topic Modeling with Latent Semantic Analysis based on Singular Value Decomposition.  
This creates song and tag features which is one way of solving the missing tag problem which is an obstacle for tag exclusion.

### How do I get set up? ###

Clone from Bitbucket: 
~~~
git clone git@bitbucket.org:funatika/tagexlusion.git
~~~
Build project with Maven: 
~~~
mvn package
~~~
Run with ant: 
~~~
ant run
~~~
See targets: 
~~~
ant -p
~~~

### Who do I talk to? ###

* Oliver Guggenberger: oliver@funatika.org
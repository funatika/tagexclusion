package feature;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import utility.CSVUtils;

public class FeatureEntry {
	//feature to which the tag/song belongs
	private int featureNr;
	//cleanID of the tag/song
	private int id;
	//tag name/ artist + song name
	private String name;
	//svd relation to the feature
	private double dominance;
	
	public FeatureEntry(int featureNr, int id, String name, double dominance){
		this.featureNr = featureNr;
		this.id = id;
		this.name = name;
		this.dominance = dominance;
	}
	
    public static void writeFeaturesEntries(String pathTagFeatures, List<FeatureEntry> tagFeatureList) {
    	System.out.println("Writing tag features to csv file: "+pathTagFeatures);
    	
        String csvFile = pathTagFeatures;
        FileWriter writer;
		try {
			writer = new FileWriter(csvFile);
	        //for header
	        CSVUtils.writeLine( writer, Arrays.asList("FeatureNr", "Id", "Name", "Dominance") );

	        for (FeatureEntry tf : tagFeatureList) {
	            List<String> list = new ArrayList<>();
	            list.add( "" + tf.getFeature() );
	            list.add( "" + tf.getId() );
	            list.add( "" + tf.getName() );
	            list.add( "" + tf.getDominance() );
	            
	            CSVUtils.writeLine(writer, list);
	        }

	        writer.flush();
	        writer.close();
	        
	        System.out.println("Done.");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    public void printFeatureEntry(){
    	System.out.println("FeatureNr: "+this.featureNr+ ", id: "+this.id + ", name: " + this.name + ", dominance: "+this.dominance );
    }
    
    
    /**************GETTER*&SETTER********************/
	public int getFeature() {
		return featureNr;
	}

	public void setFeature(int featureNr) {
		this.featureNr = featureNr;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDominance() {
		return dominance;
	}

	public void setDominance(double dominance) {
		this.dominance = dominance;
	}
	/***********************************************/
	
    //sort feature entries by dominance (Ascending). 
    public static class Comparators {
        public static final Comparator<FeatureEntry> DOMINANCE = (FeatureEntry fe1, FeatureEntry fe2) -> Double.compare(fe1.dominance, fe2.dominance);
    }
	
}

package feature;

import data.Track;

public class TagFeature extends Feature{

	public TagFeature(int featureNr) {
		super(featureNr);
	}
	
    /* 
    *  Checks if a track contains enough top tags of the given feature.
    *  A top tag is a tag which is above the given threshold.
    *  Returns true if the feature contains enough top tag.
    */
    public boolean containsTopTags( Track track, double threshold, int cleanId ) {
    	//the number of top tags a features must contain
    	int minNumberOfTags = 2;
    	//number of top tags a feature contains
    	int topTagsCounter = 0;
    	
    	for( FeatureEntry tagFeatureEntry : this.getEntries() ){ 
			if( ( tagFeatureEntry.getDominance() > threshold  && track.containsTag( cleanId ) ) || track.containSimilarTags(cleanId).size() > 1 ) topTagsCounter++;	
	    }
    	//System.out.println("Top tags "+ topTagsCounter);
    	return topTagsCounter >= minNumberOfTags ? true : false;
    }

}

package data;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/*
 * This class can handle all operations which needs informations from the SQLite Database.
 * Source: sqlitetutorial.net
 */
public class DBHandler {
	
	static Connection connection = null;
	
    public static void connect() {
        try {
            String url = "jdbc:sqlite:pers/musicnav_US.db";
            // create a connection to the database
            connection = DriverManager.getConnection(url);
            
            //System.out.println("Connection to SQLite has been established.");
        } 
        catch (SQLException e) {
            System.out.println(e.getMessage());
        } 
    }
    
    public static void disconnect(){
        try {
            if (connection != null) {
            	connection.close();
            }
        } 
        catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    
    /*
     * Populates the occurrence matrix.
     * Returns the track list with annotated tags.
     */
    public static TrackList getOccurrenceMatrix( boolean verbose ){
    	List<Track> tmpTrackList = new ArrayList<Track>();

		try {
			Statement stmt = connection.createStatement();

			ResultSet res = stmt.executeQuery("SELECT TT.TrackID, TT.TagID, TT.LastFMWeight, TAG.cleanID FROM TT "+
												" inner join TAG on TT.TagID = TAG._id ORDER BY TrackId, TagId ASC" );
			
	        int tmpTrackId = -1;
	        Track tmpTrack = null;
	        
	        //is the cleanId of the tracks. There are holes in the table between the tracks. the cleanId is without them.
	        //this step is needed because we dont want to have holes in the OccurrenceMatrix
	        int songCounter=1;
	        
	        //read result
	        while ( res.next() ) {
	        	
	            int trackId = res.getInt("TrackID");
	            int tagId  = res.getInt("cleanID");
	            int lastFmWeight  = res.getInt("LastFMWeight");          
	            
	            //first iteration
	            if( tmpTrackId == -1 ){
	            	tmpTrackId = trackId;
	            	tmpTrack = new Track( trackId );
	        		tmpTrack.setCleanId( songCounter );
	            }
	            
	        	if( tmpTrackId == trackId ){ //same track
		            //songTags.add(tagId);
		            tmpTrack.addTag( tagId, lastFmWeight );
	        		tmpTrack.setCleanId( songCounter );
	        	}
	        	else{ //new track	
		            //System.out.println("New track:");
	        		if( verbose ){
			            System.out.println( "TrackID: " + tmpTrackId );
		        		System.out.println( "CleanID: " + songCounter );    
			            System.out.println( "Num tags: "+ tmpTrack.getTagCount() );
			            System.out.print( "tags ");       
			            tmpTrack.printTags();		
			            System.out.println("\n");
	        		}
            
		            //tmpTrack.removeTag( 78 ); //remove later!!!
	        		OccurrenceMatrix.addTags( songCounter, tmpTrack.getTags() );
	        		
	        		//add track to trackList
	        		tmpTrackList.add(tmpTrack);
	        		
		            //update tmpTrackId
	        		tmpTrackId = trackId;
	        		tmpTrack = new Track(trackId);
	        		//cleanId is the id without holes in between the tracks
	        		tmpTrack.setCleanId(songCounter);
		            tmpTrack.addTag(tagId, lastFmWeight);
	        		songCounter++;
	        	}
	         }
			 System.out.println("Finished! Number of songs: "+songCounter);
			 
	         res.close();
	         stmt.close();		
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		
		return new TrackList( tmpTrackList );

    }
    
    /*
     * Returns the real id of a track by giving the cleanId.
     */
    public static int getRealTrackId(int cleanID){
    	int realId = 0;
  		try {
  			Statement stmt = connection.createStatement();
  					
  			ResultSet res = stmt.executeQuery("Select _id From Track where cleanId = "+cleanID ); 
  			realId = res.getInt("_id");
  			 
  	        res.close();
  	        stmt.close();		   
  		} 
  		catch (SQLException e) {
  			e.printStackTrace();
  		}
	    return realId;
    }
    
    /*
     * Returns the clean id of a tag by giving the realId.
     */
    public static int getCleanTagId(int realId){
    	int cleanId = 0;
  		try {
  			Statement stmt = connection.createStatement();
  					
  			ResultSet res = stmt.executeQuery("Select cleanID From Tag where _id = "+realId ); 
  			cleanId = res.getInt("cleanID");
  			 
  	        res.close();
  	        stmt.close();		   
  		} 
  		catch (SQLException e) {
  			e.printStackTrace();
  		}
	    return cleanId;
    }
    
    /*
     * Returns an array list with all tags (realId!!) of the track with the given realId
     */
    public static ArrayList<Integer> getTrackTags( int realID ){
    	ArrayList<Integer> trackTags = new ArrayList<Integer>();
  		try {
  			Statement stmt = connection.createStatement();
  					
  			ResultSet res = stmt.executeQuery("Select TagID From TT where TrackID = "+realID ); 
  			
	        while ( res.next() ) {
	        	trackTags.add( res.getInt("TagID") );
	        }
  			 
  	        res.close();
  	        stmt.close();		   
  		} 
  		catch (SQLException e) {
  			e.printStackTrace();
  		}  	
    	return trackTags;
    }
    
    /*
     * Returns an array list with all tags (realId!!) of the track with the given realId.
     */
    public static TrackList setAllTrackTags( TrackList trackList ){
  		try {
  			Statement stmt = connection.createStatement();				
  			ResultSet res = stmt.executeQuery("SELECT TT.TagID, TT.TrackID, TAG.CleanID, TAG.Name FROM TT " +
  												"inner join TAG on TT.TagID = TAG._id ORDER BY TrackId, TagId ASC"); 
  			  			
  			//counter starts with first entry from trackList
  			int counter = 0;
  			//realTrackId of first entry from the given trackList
  			Track currentTrack = trackList.getTrack(counter);
 			
	        while (  res.next() && counter < trackList.getSize() ) {  				
	        	//trackId from current table entry
	  			int trackId = res.getInt("TrackID");
	  			int cleanTagId = res.getInt("cleanID");
	  			//String name = res.getString("name");
	  			
	  			if( currentTrack.getTrackId() == trackId ) {
	  				//cleanId of tag and 0 for lastFmWeight because its not important here
	  				//System.out.println("add to old track from trackList " + counter + ", trackId "+ currentTrack.getTrackId());
	  				currentTrack.addTag( cleanTagId, 0 );
	  			}
	  			else{ //next entry in trackList
	  				int nextElemIndex = counter + 1;
	  				//next track from table is also in the trackList
	  				if ( trackList.getTrack(nextElemIndex) != null && trackList.getTrack(nextElemIndex).getTrackId() == trackId ){
	  					currentTrack = trackList.getTrack(nextElemIndex);
	  					//System.out.println("new track id from trackList "+counter+ ", trackId "+currentTrack.getTrackId());

	  					currentTrack.addTag( cleanTagId, 0 );
	  					counter++;
	  				}
	  			}	
	        }
  			 
  	        res.close();
  	        stmt.close();		   
  		} 
  		catch (SQLException e) {
  			e.printStackTrace();
  		}  	
    	return trackList;
    }
    
    /*
     * Returns: The tag name of the given cleanId of a tag.
     */
    public static String getTagName( int cleanID ){
    	String tagName = "";
  		try {
  			Statement stmt = connection.createStatement();
  					
  			ResultSet res = stmt.executeQuery("Select NAME From TAG where cleanId = "+cleanID ); 
  	        tagName = res.getString("Name");
  			 
  	        res.close();
  	        stmt.close();		   
  		} 
  		catch (SQLException e) {
  			e.printStackTrace();
  		}
	    return tagName;
    }
    
    /*
     * Returns the song name of the cleanId of a track.
     */
    public static String getSongName( int cleanID ){
    	String songName = "";
  		try {
  			Statement stmt = connection.createStatement();
  			ResultSet res = stmt.executeQuery("Select TRACK.Name, ARTIST.Name as 'ArtistNAME' From TRACK INNER JOIN ARTIST"
  										+ " on TRACK.ArtistID = ARTIST._id WHERE cleanId = "+cleanID ); 
  		
  			songName = res.getString("ArtistName");
  			songName += " - "+res.getString("Name");
  	
  	        res.close();
  	        stmt.close();		   
  		} 
  		catch (SQLException e) {
  			e.printStackTrace();
  		}
	    return songName;
    }

}

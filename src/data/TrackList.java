package data;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class TrackList {
	//list of all tracks
	private List<Track> trackList;
	
	public TrackList( List<Track> trackList ){
		this.trackList = trackList;
	}
	
	public void setTrackList(List<Track> trackList){
		this.trackList = trackList;
	}
	
	public List<Track> getTrackList(){
		return trackList;
	}
	
    //returns the total number of tracks in the track list
    public int getSize(){
    	return trackList.size();
    }   
	
    //returns the track with the at the given index
    public Track getTrack( int index ){
    	if( index < this.trackList.size() ) return this.trackList.get( index );
    	return null;
    }
    
    //sort track list by track id
	public void sortTrackListByTrackId(){
    	Collections.sort( this.trackList, Track.Comparators.TRACKID );
	}
	
    public void printTrackList(){
    	for(  Track t : trackList ){
    		System.out.println("");  
    		System.out.println("TrackID: "+ t.getTrackId() );
    		System.out.println("CleanID: "+ t.getCleanId() );
    		
    		if( t.getName().equals("") || t.getName() == null){
        		System.out.println("Name: "+ DBHandler.getSongName( t.getCleanId() ) );
    		}else{
    			System.out.println("Name: "+ t.getName() );
    		}
            System.out.print( "tags ");       
            t.printTags();
            System.out.println("\n");    
    	}
    }
    
	/*
	 * Removes tracks which occurs multiple times in tracklist
	 * returns the number of removed tracks
	 */
	public int removeDuplicates(){
		//number of removed tracks
		int removedTracks = 0;		
		
		for( int i=0; i < this.trackList.size(); i++ ) {		
			Track t = this.trackList.get( i );
			
			for ( int j=i+1; j < this.trackList.size(); j++ ) {
				if( this.trackList.get( j ).getTrackId() == t.getTrackId() ){
					this.trackList.remove( j );
					removedTracks++;
				}
			}
		}	
		return removedTracks;
	}
	
    
    //removes a specific tag from all songs of the track list
    public void removeTagFromList( int cleanId ){
    	for(  Track t : trackList ){
    		t.removeTag( cleanId );
    	}
    }
    
    
    /**********************************************************/

    /* 
     * picks random tracks from the trackList 
     * returns an ArrayList with the chosen indexes
     */
    public ArrayList<Integer> pickRandomSongs( int number){
    	//list with randomly chosen songs
    	ArrayList<Integer> pickedSongs = new ArrayList<Integer>();
    	Random rand = new Random();
 	
    	int counter=0;   	
    	while( counter < number ){
    		//pick random a index within the track list
        	int  n = rand.nextInt( trackList.size() ) + 1;
        	//add track if random song id isn't already in pickedSongs list
        	if( !pickedSongs.contains( n ) ){
        		pickedSongs.add( n );
        		counter++;
        	}
    	}	
    	return pickedSongs;
    }
    
    /*
     * Removes all tracks of the current trackList which are in the given blacklist
     */
    public int removeTracksOfBlackList(TrackList blacklist){
    	int removedTracks = 0;
    	for( int i=0; i < this.trackList.size(); i++ ){
    		//current track 
    		Track tWhite = trackList.get(i);
    		//check if it is in blacklist
    		for( Track tBlack : blacklist.getTrackList() ){
        		if( tWhite.getCleanId() == tBlack.getCleanId() ){
        			this.trackList.remove(i);
        			removedTracks++;
        		}

    		}
    	}   	
    	return removedTracks;
    }

}

package data;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import Jama.Matrix;

public class OccurrenceMatrix {
	static int numTags = 360; //357 
	static int numSongs = 93350; //93350; //1176755 //93342  //50000
	
	
	//first value is column -> tags
	//second value is row -> songs
	static double[][] om = new double [numTags][numSongs];   
	
	static String savePath = "pers/occurences.txt";

	//print occurrence matrix
	public static void print(){
		//print column number
		System.out.print("\t ");

		for(int i=0; i < numSongs; i++){
			System.out.print(i+1 + "  " );
		}
		System.out.println("\n");
		
		for(int i=0; i < numTags; i++){
			//print row numbers
			System.out.print(i+1 + ":\t ");
			
			for(int j=0; j < numSongs; j++){
				System.out.print( om[i][j] + "  " );
			}
			System.out.println();
		}
	}
	
	
	public static void showDim(){
		System.out.println(numTags+"x"+numSongs);
		System.out.println("Number of Tags:"+numTags);
		System.out.println("Number of Songs:"+numSongs);
	}
	
	public static void setNumSongs(int num){
		numSongs = num;
		om = new double [numTags][numSongs];   
	}
	
	//add tags from a specific song to occurrence matrix
	public static void addTags( int position, ArrayList<int []> tags){
		if( position >= numSongs )return;
		
		for(int i=0; i < tags.size(); i++){
			int tagId = tags.get( i )[0];
			int lastFmWeight = tags.get( i )[1];
			double normalizedWeight = (double)(lastFmWeight) / 100 ;
			om[ --tagId ][position] = normalizedWeight;
		}
	}
	
	//converts a 2d int array to a "double" matrix
	public static Matrix getMatrix(){
	
		double[][] array = new double[numTags][numSongs];
		 
		for(int i=0; i < numTags; i++){
			
			for(int j=0; j < numSongs; j++){
				array[i][j] = om[i][j];
			}
		}
		return new Matrix(array);
	}
	
	
	//saves occurrence matrix in dense text format to file
	public static void save(){	
		
		try( PrintWriter pwriter = new PrintWriter( savePath )  ){
			
			//write dimensions in the first line
			pwriter.println( numSongs+" "+numTags );
			
			for(int i=0; i < numTags; i++){		
				
				String line = "";
				
				for(int j=0; j < numSongs; j++){
					line = line + om[i][j] + " ";
				}
				System.out.println(i+":  "+line);
				pwriter.println(line);
			}	
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	//saves occurrence matrix in dense text format to file
	public static void saveDt(){			
		try( PrintWriter pwriter = new PrintWriter( savePath )  ){		
			int nSongs = om[0].length;
			int nTags = om.length;
			System.out.println( nSongs+" "+nTags );
					
			//number of all values in the matrix which are not null
			int notNullAll = 0;
			String content = "";
			
			for(int i=0; i < nSongs; i++){	
				String line = "";
				//counts all values which are not 0 from a column
				int notNull= 0;
				
				//iterate over the column
				for(int j=0; j < nTags; j++){
					//System.out.println("i: "+ i+" j: "+j );

					//save value of occurrence matrix in sparse text format only if value is bigger than null
					if( om[j][i] > 0 ){
						line = line + j + " " + om[j][i] + "\n";
						notNull++;
					}	
					notNullAll = notNullAll + notNull;
				}			
				line = notNull + "\n"+line;
				content = content + line;
				System.out.print(line);
			}		
			pwriter.println( nSongs+" "+nTags+" "+notNullAll );
			pwriter.print(content);	
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	
	//loads occurence matrix from file
	public static void load(){
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader( savePath ));		
			
			String line;
			int counter=0;
			while((line = in.readLine()) != null){
				//every 2nd element is a empty string
			    String [] values = line.split(" ");
			    
			    for(int i=0; i < values.length; i++){
			    	//filter empty strings
			    	if( values[i] != " " && values[i] != "" ){
				    	//parse string to int and assign all values from the actual line to the om matrix
			    		om[counter][i] =  Double.parseDouble( values[i] );
			    	}    	
			    }
			    counter++; 
			}
			in.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}
	
	
	public static void loadDt(){
		BufferedReader in;
		try {
			in = new BufferedReader(new FileReader( savePath ));		
			
			String line;
		    String [] dimension = in.readLine().split(" ");
		    
		    if( dimension.length == 2){
		    	numSongs = Integer.parseInt( dimension[0] );
		    	numTags = Integer.parseInt( dimension[1] );
		    }else{
		    	in.close();
		    	return;
		    }
		   
		    int activeCol = 0;
			while((line = in.readLine()) != null){
				//number of values which are not null from the actual column
				int colValuesNotNull = Integer.parseInt( line  );
			    
			    for(int i=0; i < colValuesNotNull; i++){
				    //key value pair. first value is the position of the value, second is the value
			    	if( (line = in.readLine()) != null ){
					    String [] keyValuePair = line.split(" ");
					    
					    if( keyValuePair.length == 2 ){
						    int posValue = Integer.parseInt( keyValuePair[0]  );
						    double value = Double.parseDouble( keyValuePair[1]  );
						    
						    om[posValue][activeCol] = value;
						    //System.out.println( "pos "+posValue+"  value "+value +"  activeCol "+activeCol);		   
					    }
			    	}
			    }
			    activeCol++;
			}
			in.close();
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch (IOException e) {
			e.printStackTrace();
		}

	}
	

}

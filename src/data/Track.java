package data;
import java.util.ArrayList;
import java.util.Comparator;


public class Track {
	//track id is the real id
	private int trackId;
	//is the id without holes
	private int cleanId;
	
	private String name;
	private double dominance;
	private int featureNr;

	//tags of the song [ structure is: array list with int array for tagNr; lastFmWeight ]
	ArrayList<int []> tags;
	
	public Track(int trackId){
		this.trackId = trackId;
		this.tags = new ArrayList<int []>();
	}
	
	public void addTag(int tagId, int lastFmWeight){
		//only add if tag is not already in the list
		if( !tags.contains( tagId ) ){ 
			int [] tagLFM = new int [2];
			tagLFM[0] = tagId;
			tagLFM[1] = lastFmWeight;
			tags.add(tagLFM);
		}
	}
	
	public ArrayList<int []> getTags(){
		return tags;
	}
	
	
	public void removeTag( int cleanId ){
		for( int i=0; i < tags.size(); i++ ){
			if( this.tags.get(i)[0] == cleanId ){
				this.tags.remove(i);
			}
		}
	}
		
	//determine if a tag is annotated on a track or not
	public boolean containsTag( int cleanId ){
		for( int i=0; i < tags.size(); i++ ){
			if( this.tags.get(i)[0] == cleanId ){
				return true;
			}
		}
		return false;
	}
	
	//determine if a song contains similar tags ( e.g. Song A contains Rock but also Pop Rock and Punk Rock. )
	//returns an array with similar tags. if size of returned list is 0 -> no similar tags
	public ArrayList<Integer> containSimilarTags( int cleanId ){
		//list with similar tags
		ArrayList<Integer> similarTags = new ArrayList<Integer>();
		
		String tagName = DBHandler.getTagName( cleanId );
		
		for(int i=0; i< tags.size(); i++){
			String tmpTagName = DBHandler.getTagName( tags.get(i)[0] );
			if( tmpTagName.contains( tagName) ){
				similarTags.add( tags.get(i)[0]  );
			}
		}
		return similarTags;
	}

	//show tag names without LastFmWeight
	public void printTags(){
		System.out.print( "[ ");
		for(int i=0; i< tags.size(); i++){
			System.out.print( tags.get(i)[0] +", " );
		}
		System.out.print( " ]");
	}
	
	
    //sort by trackId
    public static class Comparators {
        public static final Comparator<Track> TRACKID = (Track t1, Track t2) -> Integer.compare(t1.trackId, t2.trackId);
    }    
	
	//**********GETTER & SETTER*************
	
	//returns number of tags
	public int getTagCount(){
		return tags.size();
	}

	public int getCleanId() {
		return cleanId;
	}
	
	public void setCleanId( int cleanId ){
		this.cleanId = cleanId;
	}
	
	public int getTrackId(){
		return trackId;
	}	
	
	public void setTrackId( int newTrackId ){
		this.trackId = newTrackId;
	}	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getDominance() {
		return dominance;
	}

	public void setDominance(double dominance) {
		this.dominance = dominance;
	}

	public int getFeatureNr() {
		return featureNr;
	}

	public void setFeatureNr(int featureNr) {
		this.featureNr = featureNr;
	}

}

package statistic;

import java.util.ArrayList;
import data.DBHandler;
import data.Track;
import data.TrackList;

public class Statistics {
	//id of the tag which should be excluded
	int cleanExTagId;
	int mean;
	//number of iterations for the mean value
	int iterations = 2;
	//size of the random sublist of the track list	
	int numRandomSongs = 5000;
	boolean countSimilarTags = true;

	//list of randomly picked indexes from the track list. 
	
	ArrayList<Integer> sublist;
	TrackList trackList;	
	
	public Statistics( TrackList trackList, int cleanExTagId )  {
		this.trackList = trackList;
		this.cleanExTagId = cleanExTagId;
		init();
	}
	
	private void init(){
		this.sublist = this.trackList.pickRandomSongs( this.numRandomSongs );
		this.mean = this.getMeanValue( this.iterations );
	}
	
	public int getCleanExTagId() {
		return cleanExTagId;
	}

	public void setCleanExTagId(int cleanExTagId) {
		this.cleanExTagId = cleanExTagId;
		//reinitialize for new params
		init();
	}

	public int getNumRandomSongs() {
		return numRandomSongs;
	}

	public void setNumRandomSongs(int numRandomSongs) {
		this.numRandomSongs = numRandomSongs;
		//reinitialize for new params
		init();
	}

	public boolean isCountingSimilarTags() {
		return countSimilarTags;
	}

	public void setCountingSimilarTags(boolean countSimilarTags) {
		this.countSimilarTags = countSimilarTags;
		//reinitialize for new params
		init();
	}

	public int getMeanValue(int iterations){
		for(int i=0; i < iterations; i++){
			mean = mean + countTagOccurrenceSublist( );
		}
		this.mean = mean / iterations;
		return mean;
	}
	
    /* counts the occurrence of a track in the whole track list */
    public int countTagOccurrence( ){
    	int tagCount = 0;
    	for( Track t : trackList.getTrackList() ){
    		
        	if( countSimilarTags ){      		
	    		if( t.containsTag( cleanExTagId ) || t.containSimilarTags( cleanExTagId ).size() > 0 ){
	    			tagCount++;
	    		}
        	}
        	else { //only count the specified tag
        		if( t.containsTag( cleanExTagId )  ){ 
        			tagCount++;
        		}
        	}
    	}
    	return tagCount;
    }
	
    /*
     * counts the occurrences of a tag in the given (sub) list
     * int cleanId is the id of the tag which should be counted
     */
    public int countTagOccurrenceSublist( ) {
    	int tagCount = 0;
    	//randomId is the actual element id of the sublist
    	for( int i=0; i < sublist.size(); i++ ){   		
    		//in the sublist are the indexes which are chosen from the track list
    		int pickTrackIndex = sublist.get( i );
    		//get track at random position/index
        	Track t = trackList.getTrack( pickTrackIndex );
        	
        	if( t != null ) {
       	
	        	if( countSimilarTags ){
	            	//if track contains this or a similar tag, increase the counter
	            	if( t.containsTag( cleanExTagId ) || t.containSimilarTags( cleanExTagId ).size() > 1 ){
	            		tagCount++;
	            	}
	        	}
	        	else{ //only count the specified tag
	            	if( t.containsTag( cleanExTagId )  ){
	            		tagCount++;
	            	}
	        	}
        	}
    	}
    	return tagCount;
    }
    
    
    public void printStatistics( ) {
    	String tagName = DBHandler.getTagName( this.cleanExTagId ) ;
    	
    	//------------whole track list-------------
    	System.out.println("\nStatistics for the Tag '" + tagName + "' with the id: " + this.cleanExTagId + "\n");
    	System.out.println("Number of tracks in the track list: " + trackList.getSize() );
    	System.out.println("Tag '" + tagName + "' occurres " + countTagOccurrence( ) + " times in the track list" );  
    	
    	
    	//------------sublist-of-randomly-picked-tracks---------
    	System.out.println("\nSublist statistics");
    	System.out.println("Number of tracks in the sublist of totally " + this.numRandomSongs );
    	System.out.println("Tag '" + tagName + "' occurres " +  this.countTagOccurrenceSublist( ) + " times in the track list" );  
		//proportion
		double propSub =  ( 100 *  (double)(this.mean) )  /  (double)(this.numRandomSongs);
		System.out.println("Occurrence in sublist: " + propSub + "%");		
    	System.out.println("Counted similar tags: "+this.countSimilarTags );
    	System.out.println("Mean value calculated by: "+this.iterations +" iterations, mean value is: "+this.mean );
		System.out.println();
    }
    
    
    
}

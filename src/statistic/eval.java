package statistic;
import java.util.ArrayList;
import java.util.List;
import data.DBHandler;
import data.Track;
import data.TrackList;
import feature.Feature;
import feature.FeatureEntry;
import feature.TagFeature;
import utility.CSVReader;

/*
 * This class evaluates the performance of the implemented topic modeling
 */
public class eval {
	
	//path of the CSV tag feature file and track feature file
	static String csvTagFeaturesPath = "pers/csv/tagFeatures_threshold_1.0.csv";
	static String csvTrackFeaturesPath = "pers/csv/songFeatures_threshold_0.5.csv";	
	//list of all tag features
	static List<TagFeature> tagFeatures;
	//list of all tracks of all features above the threshold value from the filename
	static TrackList trackList;	
	static double thresholdTopTags = 0.3;
	//id of the tag which should be excluded from the recommendations
	static int excludedTag = 78; //clean id
	static boolean countSimilarTags = true;
	static boolean skipFeatureNull = false;
	
    public static void main(String[] args) {	   	
    	System.out.println("Starting evaluation...");
    	System.out.println();
    	DBHandler.connect();
    	
    	//*************TAG*FEATURES********************************	
    	
    	//get tag features from the csv file, skip feature 0
    	tagFeatures = CSVReader.readTagFeaturesFromCSV( csvTagFeaturesPath, skipFeatureNull );
    	
    	//sort tag features by dominance
    	for( Feature f : tagFeatures ){
    		f.sortFeatureEntriesByDominance();
    	}
    	
    	//************TRACK*FEATURES*******************************

    	//get tag features from the csv file, skip feature 0, fix realIds of tracks
    	trackList = new TrackList( CSVReader.readTrackListFromCSV( csvTrackFeaturesPath, skipFeatureNull, true ) );    	   	
    		    	 	    	
    	trackList.removeDuplicates();    	
    	trackList.removeDuplicates();    	
    	trackList.sortTrackListByTrackId();
    	  	
    	//-------ADDING TAGS------
    	//System.out.println("Add tags..."); 	 	
    	
    	//add tags to track list
    	trackList = DBHandler.setAllTrackTags( trackList ); 	
    	//trackList.printTrackList();
   	
    	
    	System.out.println("Find relevant features for the given tag id"); 	
    	//represents a list of features where the given tag is dominant
    	//a list of features to which the tag belongs
    	//find tag features where the dominance of the given tag cleanId is above the threshold
    	List<Integer> topTagFeatures = findRelevantTagFeatures( excludedTag, thresholdTopTags, skipFeatureNull );	

    	
    	//print statistics for random results
    	Statistics stats = new Statistics( trackList, excludedTag );
    	stats.setCountingSimilarTags(false);
    	stats.printStatistics( );
  	    	
    	
    	
    	/***************Exclusion list*******************/   	
    	
    	System.out.println("Create exclusion list"); 	
    	
    	//list of excluded tracks
    	List<Track> exclusionList = new ArrayList<Track>();
    	
    	//create the exclusion list (blacklist)
    	boolean verbose = false;
    	for( Track track : trackList.getTrackList() ){
			for( int featureNr : topTagFeatures ){
	    		if( track.getFeatureNr() == featureNr ){
	    			//if the current track from the track list is considered as related to a relevant feature	    			
	    			if( tagFeatures.get( featureNr ).containsTopTags( track, thresholdTopTags, excludedTag ) ){ 
	    				//add to exclusion list
	    				exclusionList.add( track );
	    				
	    				if( verbose ){
		        			System.out.println("\n"+ track.getCleanId() + ", " + track.getTrackId()+ ", "+track.getName() );
		        			track.printTags();
		        			System.out.println();	
	    				}

	    			}
	    		}
			}	
    	}	
    	System.out.println("Tracks in the exclusion list: " + exclusionList.size() ); 	
    	   	    	
    	//CREATE NEW LIST WITH ALL TRACKS AND EXCLUDE TRACKS FROM EXCLUDE LIST   	
    	//verbose false -> no output messages
    	TrackList recommendationList = trackList; 
    	//DBHandler.getOccurrenceMatrix(false);	 //get all tracks from database
    	TrackList blackList = new TrackList(exclusionList);
    	
    	//print exclusion list
    	//System.out.println("ExclusionList");
    	//blackList.printTrackList();  	
    	//System.out.println("\nRecommendation list size " + recommendationList.getSize() + ", exlusion list size "+ exclusionList.size() ); 	
    	
    	System.out.println("Remove the tracks which are in the exclusion list from recommendation list");
    	//remove tracks from the blacklist from recommendationList and print the number of removed tracks
    	System.out.println( "Removed tracks "+recommendationList.removeTracksOfBlackList( blackList ) );  
    	System.out.println("Recommendation list size " + recommendationList.getSize() ); 	
    	//recommendationList.printTrackList();
    	System.out.println();  	
  
    	//print statistics for recommendation list
    	Statistics recommendations = new Statistics( recommendationList, excludedTag );
    	recommendations.printStatistics();
    }
       

    
    /* 
     * Find features where the given cleanTagId is dominant. 
     * The featureList contains a list of feature id's (number) to which the given Tag belongs.
     * returns a list with the feature numbers
    */ 
	public static List<Integer> findRelevantTagFeatures( int cleanTagId, double threshold, boolean skipFeatureNull ){
		List<Integer> featureList = new ArrayList<Integer>();

    	//get tag name of the given cleandId
    	String tagName = DBHandler.getTagName(cleanTagId).toLowerCase();
    	
		if( tagFeatures.size() > 0 ){
			
			for( TagFeature tagFeature : tagFeatures){
				
				//check all entries of the current feature
		    	for( FeatureEntry tagFeatureEntry : tagFeature.getEntries() ){
		    		//skip feature 0
		    		if( tagFeatureEntry.getName().toLowerCase().equals( tagName ) ){
		    			if ( ( skipFeatureNull &&  tagFeatureEntry.getFeature() != 0 ) || skipFeatureNull == false){
			    			if( tagFeatureEntry.getDominance() > threshold ){
			    				featureList.add( tagFeatureEntry.getFeature() );	
			    				//System.out.println("Feature " + tagFeatureEntry.getFeature()+ " contains this tag with the required minimum dominance. Dominance "+ tagFeatureEntry.getDominance() );
			    			}
		    			}
		    		}
		    	}
			}
		}else{
			System.out.println("No tag features available");
		}
		return featureList;
	}
    
	
    /* 
     * add tags to the track of the given featureList 
     * This creates for every track a new db conntection and fetch the annotated tags for a given track. 
     * Do not use this method for a big list
     * boolean addOnlyToRelevantFeatureTracks specify if the tags should be added to non relevant features
     */  
    public static void addTags(List<Integer> featureList, boolean addOnlyToRelevantFeatureTracks){
    	
    	for( Track t : trackList.getTrackList() ){
    		int featureNr = t.getFeatureNr();   		
    		
    		if( featureList.contains( featureNr ) || addOnlyToRelevantFeatureTracks == false ){
    		
        		int realTrackId = t.getTrackId();
        		
        		//System.out.println("Get track tags for track with realId: "+realTrackId);
            	ArrayList<Integer> trackTags = DBHandler.getTrackTags( realTrackId );
            	
            	for( int realTagId : trackTags ){
            		int cleanTagId = DBHandler.getCleanTagId( realTagId );
            		//add tag with the proper cleanId, second param is the lastFMWeight wich is not important here
            		t.addTag(cleanTagId, 0);
            	}		
    		}
    	} 	
    }
    
}

package main;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.math3.linear.SingularValueDecomposition;
import org.apache.commons.math3.linear.SparseRealMatrix;

import Jama.Matrix;
import data.DBHandler;
import data.OccurrenceMatrix;
import data.TrackList;
import feature.FeatureEntry;
import utility.SVD;
import utility.Utility;

/*
 * Command Line Interface for creating the occurrence matrix, calculating the Singular Values and some methods for saving and loading data.
 */

public class run {
	//converted occurrence matrix
	static Matrix A;
	static Matrix U;
	//relation features and tags
	static Matrix US;
	//singular values (features)
	static Matrix S;
    static Matrix Features;

	static Matrix V;
	//relation features and songs
	static Matrix VS;
	
	static TrackList trackList;
	
	static double threshold=1;
	
	//specify the format of the files 
	static boolean denseTextFormat = true;

	/*
	 * REPL:
	 * 	-Repeat
	 * 	-Evaluates
	 *  -Print
	 *  -Loop
	 */
	public static void main(String []args){
		System.out.println("Enter a command (h for help): ");
		Scanner sc = new Scanner(System.in);
		Scanner subCmd; //input for sub commands => e.g command: set threshold; second input value is the "sub command"
		boolean run = true;
		
		while( run ){
			String cmd = sc.nextLine();
			
			if( cmd.equals("h") ){
				usage();
			}else if( cmd.equals("q") ){
				run = false;
				System.out.println("exit");
			}
			else if( cmd.equals("populate") ){
				System.out.println("Populating occurrence matrix");
				populateOM();
			}
			else if( cmd.equals("save") ){
				System.out.println("Saving occurrence matrix");
								
				if( denseTextFormat ){
					OccurrenceMatrix.saveDt();
				}
				else{
					OccurrenceMatrix.save();
				}
			}
			else if( cmd.equals("load") ){
				System.out.println("Load the occurrence matrix");
				if( denseTextFormat ){
					OccurrenceMatrix.loadDt();
				}
				else{
					OccurrenceMatrix.load();
				}
			}
			else if( cmd.equals("print") ){
				System.out.println("Print occurence matrix");
				OccurrenceMatrix.print();
			}
			else if( cmd.equals("dim") ){
				OccurrenceMatrix.showDim();
			}
			else if( cmd.equals("calc svd") ){
				System.out.println("Compute the singular values");
				
				//messure needed time
				long startTime = System.nanoTime();
				calculateSVD();
				long endTime = System.nanoTime();

				long duration = (endTime - startTime) / 1000000; //milisec
				duration = duration / 1000; //sec
				//System.out.print("\007"); //play ascii beep sound
				long min = duration/60; //mi8n
				System.out.println( "\nTime needed:\n"+min+ " min");
				System.out.println( duration+ " sec\n");
			}
			else if( cmd.equals("show svd") ){
				showSVD();
			}
			else if( cmd.equals("save svd") ){
				saveSVD();
			}	
			else if( cmd.equals("load svd") ){
				loadSVD();
			}	
			else if( cmd.equals("features") ){
				showFeatures();
			}
			else if( cmd.equals("show threshold") ){
				System.out.println("Threshold value is: "+threshold);
			}
			else if( cmd.equals("set threshold") ){
				System.out.println("Enter a value for the ne threshold value: ");
				
				subCmd = new Scanner( sc.nextLine() );
				double newThreshold = subCmd.nextDouble();
				threshold = newThreshold;
				System.out.println("New threshold value is now: "+threshold);
				
			}
			else if( cmd.equals("song features") ){
				System.out.println("Show all song belonging to the first n features.");
				System.out.println("Enter now the number of feature you want to see:");
				subCmd = new Scanner( sc.nextLine() );

				int featureNr = subCmd.nextInt();
				songFeatures(featureNr);				
			}
			else if( cmd.equals("tag features") ){
				System.out.println("Show all tags belonging to the first n features.");
				System.out.println("Enter now the number of feature you want to see:");
				
				subCmd = new Scanner( sc.nextLine() );
				int featureNr = subCmd.nextInt();
				tagFeatures(featureNr);
			}	
			else if( cmd.equals("export tag features") ){
				System.out.println("Export all tags belonging to the first n features.");
				System.out.println("Enter now the number of feature you want to export:");
				subCmd = new Scanner( sc.nextLine() );

				int featureNrExport = subCmd.nextInt();
				exportTagFeatures(featureNrExport);
			}
			else if( cmd.equals("export song features") ){
				System.out.println("Export all songs belonging to the first n features.");
				System.out.println("Enter now the number of feature you want to export:");
				subCmd = new Scanner( sc.nextLine() );

				int featureNrExport = subCmd.nextInt();
				exportSongFeatures(featureNrExport);
			}
			else if( cmd.equals("set num songs") ){
				System.out.println("Enter a new value for the number of songs in the Occurrence Matrix");
				subCmd = new Scanner( sc.nextLine() );

				int numSongs = subCmd.nextInt();
				OccurrenceMatrix.setNumSongs(numSongs);
			}
			else if( cmd.equals("change format") ){
				if( denseTextFormat ){
					denseTextFormat = false;
					System.out.println("Matrix format is now in sparse text format");
				}else{
					denseTextFormat = true;
					System.out.println("Matrix format is now in sparse text format");
				}			
			}
			else{
				System.out.println("Command not recognized");
			}
			System.out.println(">");
		}	
		sc.close();
	}
	/*
	 * Shows the manual for this REPL.
	 */
	public static void usage() {
		System.out.println("Usage:");
		System.out.println("h\t\t\t for this help");
		System.out.println("q\t\t\t exit");
		
		System.out.println("populate\t\t populate occurrence matrix");
		System.out.println("save\t\t\t save occurrence matrix");
		System.out.println("load\t\t\t load occurrence matrix");
		System.out.println("print\t\t\t print occurrence matrix");
		System.out.println("dim\t\t\t show dimension for occurrence matrix");
		System.out.println("set num songs\t\t set the number of songs in the occurrence matrix (Attention this will delete the Occurence Matrix)");
		
		System.out.println("calc svd\t\t compute the singular values");
		System.out.println("show svd\t\t shows the singular values");
		System.out.println("save svd\t\t saves the svd matrices");
		System.out.println("load svd\t\t loads the svd matrices");
		
		System.out.println("features\t\t shows the features (singular values)");
		System.out.println("song features\t\t shows the songs of the first n features");
		System.out.println("tag features\t\t shows the tags of the first n features");
		
		System.out.println("export tag features\t exports the tags of the first n features, above the threshold, as CSV");
		System.out.println("export song features\t exports the songs of the first n features, above the threshold, as CSV");
		
		System.out.println("show threshold\t\t shows the threshold value");
		System.out.println("set threshold\t\t sets the threshold value");
			
		System.out.println("change format\t\t toggle the matrix format( Sparse Text Format | Dense Text Format)");
	}

	/*
	 * Calculates the Singular Values from the Occurrence Matrix.
	 */
	public static void calculateSVD(){
		//get matrix from occurrence matrix
		A = OccurrenceMatrix.getMatrix();
		
		//convert jama matrix to sparse real matrix
		SparseRealMatrix Atmp = Utility.convertJamaToSparseRealMatrix( A );
		
		System.out.println("Getting singular values");
		SingularValueDecomposition svd = new SingularValueDecomposition(Atmp);
		
		
		System.out.println("Feature Matrix");
	    Features = new Matrix(svd.getSingularValues(), 1);		
	    
	    
	    System.out.println("Getting S");
		S = Utility.convertRealMatrixToJamaMatrix( svd.getS() );
		
		System.out.println("Getting U");
		U = Utility.convertRealMatrixToJamaMatrix( svd.getU() );
		
		System.out.println("Getting V");
		V = Utility.convertRealMatrixToJamaMatrix( svd.getV() );
		
		System.out.println("Calc VS and US");
	    VS = V.times(S); 
	    US = U.times(S);
	}
	
	/*
	 * Shows the Singular Values and the decomposed matrices.
	 */
	public static void showSVD(){
/*		//show all matrices; commented due to performance reasons
  		if( S != null && U != null && V!= null){
		      System.out.println("A = U S V^T");
		      System.out.println();
		      System.out.print("U = ");      
		      U.print(6, 4);
		      
		      System.out.print("Sigma = ");
		      S.print(6, 4);
		      
		      System.out.print("V = ");
		      V.print(6, 4);  
		        
		      System.out.println("VS");
		      VS.print(6, 4);
		    
		      System.out.println("US");
		      US.print(6, 4);
*/
		//show only important ones because the output is extremely huge
		if( US != null && VS != null ){
		      System.out.println("VS");
		      VS.print(6, 4);
		    
		      System.out.println("\n\nUS");
		      US.print(6, 4);
		}else{
		      System.out.println("You have to calculate the SVD's before.");
		}
	}
	/*
	 * Print out the S matrix
	 */
	public static void showFeatures(){
		if( Features == null ){
			System.out.println("No features calculated yet!");
			return;
		}
		
		double [][] tmpArr = Features.getArray();		
		int numFeatures = tmpArr[0].length;
	     
	    System.out.println("Features above the threshold value ("+threshold+")");
	     
	    int count=0;
		for(int i=0; i < numFeatures; i++){		
			if( tmpArr[0][i] > threshold){
				System.out.println( tmpArr[0][i] );
				count++;
			}
		}	
	    System.out.println("Number of features above the threshold value: "+count);
	}
	
	/*
	 * Saves Singular Values, the VS and US matrices.
	 * Two possible formats for saving the matrices: Spare Text Format or Dense Text Format.
	 */
	public static void saveSVD(){
	    System.out.println("Saving SVDs");
	    
	    if( denseTextFormat ){
	    	//commented due to performance reasons
	    	//Utility.saveMatrixDt( A, "svd/A" );
	    	//Utility.saveMatrixDt( U, "svd/U" );
	    	//Utility.saveMatrixDt( V, "svd/V" );
	    	
	    	Utility.saveMatrixDt( S, "pers/svd/S" );
	    	Utility.saveMatrixDt( VS, "pers/svd/VS" );
	    	Utility.saveMatrixDt( US, "pers/svd/US" );
	    	
	    }else{
	    	//Utility.saveMatrix( A, "svd/A" );
	    	//Utility.saveMatrix( U, "svd/U" );
	    	//Utility.saveMatrix( V, "svd/V" );

	    	Utility.saveMatrix( S, "pers/svd/S" );
	    	Utility.saveMatrix( VS, "pers/svd/VS" );
	    	Utility.saveMatrix( US, "pers/svd/US" );
	    }
	}
	
	
	/*
	 * Loads Singular Values, the VS and US matrices from file.
	 * Two possible formats for loading the matrices: Spare Text Format or Dense Text Format.
	 */
	public static void loadSVD(){
	    System.out.println("Loading SVDs");
	    
	    if( denseTextFormat ){
	    	//commented due to performance reasons and not needed explicit
		    //A = Utility.loadMatrixDt("svd/A");
		    //U = Utility.loadMatrixDt("svd/U");
		    //V = Utility.loadMatrixDt("svd/V");

		    S = Utility.loadMatrixDt("pers/svd/S");		    
		    VS = Utility.loadMatrixDt("pers/svd/VS");
		    US = Utility.loadMatrixDt("pers/svd/US");		    
	    }else{
		    //A = Utility.loadMatrix("svd/A");
		    //U = Utility.loadMatrix("svd/U");
		    //V = Utility.loadMatrix("svd/V");
	    	
		    S = Utility.loadMatrix("pers/svd/S");
		    VS = Utility.loadMatrix("pers/svd/VS");
		    US = Utility.loadMatrix("pers/svd/US");	
	    }
	    
	    //extract singular values rather than recalculate them
	    Features = new Matrix( SVD.getSingularValuesFromS(S), 1);
	}
	
	
	
	/**********************Song*Features*********************/

	/*
	 * Shows the song features.
	 */
	public static void songFeatures(int featureNr){
		if( VS == null ) {
			System.out.println("No features calculated yet!");
			return;
		}	
		double [][] songFeatures = VS.getArray();

		if( songFeatures.length < featureNr ){
			System.out.println( "n musst be smaller than the number of total features" );
			return;
		}
		
		System.out.println("Show " + featureNr + " features.");
		DBHandler.connect();

		int showFeature=0;
		while( showFeature < featureNr){
			System.out.println("\nFeature " + showFeature+": \n");

			//i represents the cleanID of a song. 
			for(int i=0; i < songFeatures.length; i++){			
				if( songFeatures[i][showFeature] > threshold){
					int cleanID = i+1;
					System.out.println( "Song nr: "+ cleanID + " - "+ DBHandler.getSongName( cleanID ) + "  (" + songFeatures[i][showFeature] + ")" );
				}
			}
			showFeature++;
		}	
		DBHandler.disconnect();
	}
	
	/*
	 * Exports the song features to a CSV file.
	 */
	public static void exportSongFeatures(int featureNr){
		if( VS == null ) {
			System.out.println("No features calculated yet!");
			return;
		}	
		//tmp just for feature a
		double [][] songFeatures = VS.getArray();

		if( songFeatures.length < featureNr ){
			System.out.println( "n musst be smaller than the number of total features" );
			return;
		}
		
		System.out.println("Export " + featureNr + " features.");
		DBHandler.connect();

		int showFeature=0;
		//list with all tags which belongs to features above the threshold -> this list is going to be exported as CSV
    	List<FeatureEntry> songFeatureList = new ArrayList<FeatureEntry>();
    		
		while( showFeature < featureNr){

			for(int i=0; i < songFeatures.length; i++){			
				if( songFeatures[i][showFeature] > threshold){
					//cleanID starts from 1. therefore it shifted
					int cleanID = i+1;
					FeatureEntry tagFeatureEntry = new FeatureEntry( showFeature, cleanID , DBHandler.getSongName( cleanID ), songFeatures[i][showFeature] );
					songFeatureList.add( tagFeatureEntry  );
				}
			}
			showFeature++;
		}	
		
    	String pathTagFeatures = "pers/csv/songFeatures_" + "threshold_" + threshold + ".csv";
    	FeatureEntry.writeFeaturesEntries( pathTagFeatures, songFeatureList );
    	
		DBHandler.disconnect();
	}
	
	/*****************************************************/
	
	
	/****************Tag*Features*************************/
	
	/*
	 * Shows the tag features.
	 */
	public static void tagFeatures(int featureNr){
		if( US == null ) {
			System.out.println("No features calculated yet!");
			return;
		}	
		//tmp just for feature a
		double [][] tagFeatures = US.getArray();
		
		if( tagFeatures.length < featureNr ){
			System.out.println( "n musst be smaller than the number of total features" );
			return;
		}

		System.out.println("Show "+featureNr+" features.");
		DBHandler.connect();

		int showFeature=0;
		while( showFeature < featureNr){
			System.out.println("\nFeature "+showFeature+": ");

			for(int i=0; i < tagFeatures.length; i++){			
				if( tagFeatures[i][showFeature] > threshold){
					//cleanID starts from 1. therefore it shifted
					int cleanID = i+1;
					System.out.println( "Tag nr: " + cleanID + " - "+ DBHandler.getTagName(cleanID)  + "  (" + tagFeatures[i][showFeature] + ")" );
				}
			}
			showFeature++;
		}	
		DBHandler.disconnect();
	}
	
	/*
	 * Exports the tag features to a CSV file.
	 */
	public static void exportTagFeatures(int featureNrToExport){
		if( US == null ) {
			System.out.println("No features calculated yet!");
			return;
		}	
		//tmp just for feature a
		double [][] tagFeatures = US.getArray();

		if( tagFeatures.length < featureNrToExport ){
			System.out.println( "n musst be smaller than the number of total features" );
			return;
		}
		
		System.out.println("Export " + featureNrToExport + " features.");
		DBHandler.connect();

		int showFeature=0;
		//list with all tags which belongs to features above the threshold -> this list is going to be exportet as csv
    	List<FeatureEntry> tagFeatureList = new ArrayList<FeatureEntry>();

		while( showFeature < featureNrToExport){

			for(int i=0; i < tagFeatures.length; i++){			
				if( tagFeatures[i][showFeature] > threshold){
					//cleanID starts from 1. therefore it shifted
					int cleanID = i+1;
					FeatureEntry tagFeatureEntry = new FeatureEntry( showFeature, cleanID, DBHandler.getTagName( cleanID ), tagFeatures[i][showFeature] );
					tagFeatureList.add( tagFeatureEntry  );
				}
			}
			showFeature++;
		}	
		
    	String pathTagFeatures = "pers/csv/tagFeatures_" + "threshold_" + threshold + ".csv";
    	FeatureEntry.writeFeaturesEntries( pathTagFeatures, tagFeatureList );
    	
		DBHandler.disconnect();		
	}
	
	/*****************************************************/

	
	/*
	 * Connects to database and populate occurrence matrix.
	 * Saves the tracks with tag annotations to the tracklist
	 */
	public static void populateOM(){
		DBHandler.connect();
		//execute select and populate occurrence matrix
		trackList = DBHandler.getOccurrenceMatrix(true);
		
		DBHandler.disconnect();
	}
	
	
}


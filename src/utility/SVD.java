package utility;
import Jama.Matrix;
//import Jama.SingularValueDecomposition;

public class SVD {
	
	//extracts the singular values from s (diagonal array)
	public static double [] getSingularValuesFromS(Matrix S){
		double [][] tmpArr = S.getArray();
		double [] singularValuesArr = new double [tmpArr.length];
		
		for(int i=0; i < tmpArr.length; i++){
			singularValuesArr[i] = tmpArr[i][i];	
		}
		return singularValuesArr;
	}
	

}
   
 



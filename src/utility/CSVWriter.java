package utility;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import feature.FeatureEntry;

import java.io.FileWriter;
import java.io.IOException;


//Source: https://www.mkyong.com/java/how-to-export-data-to-csv-file-java/
public class CSVWriter {
	static String pathTagFeatures = "csv/tagFeatures.csv";

/*	
    public static void main(String[] args) throws Exception {
    	
    	List<FeatureEntry> tagFeatures = new ArrayList<FeatureEntry>();
    	tagFeatures.add( new FeatureEntry(1, 3, "Hip Hop", 3.84)  );
    
    	writeFeaturesEntries( pathTagFeatures, tagFeatures );
    }
*/
	
    public static void writeFeaturesEntries(String pathTagFeatures, List<FeatureEntry> tagFeatureList) {
    	System.out.println("Writing tag features to csv file: "+pathTagFeatures);
    	
        String csvFile = pathTagFeatures;
        FileWriter writer;
		try {
			writer = new FileWriter(csvFile);
	        //for header
	        CSVUtils.writeLine( writer, Arrays.asList("FeatureNr", "Id", "Name", "Dominance") );

	        for (FeatureEntry tf : tagFeatureList) {
	            List<String> list = new ArrayList<>();
	            list.add( ""+tf.getFeature() );
	            list.add( ""+tf.getId() );
	            list.add( ""+tf.getName() );
	            list.add( ""+tf.getDominance() );
	            
	            CSVUtils.writeLine(writer, list);
	        }

	        writer.flush();
	        writer.close();
	        
	        System.out.println("Done.");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

}

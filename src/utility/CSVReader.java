package utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import data.DBHandler;
import data.Track;
import feature.FeatureEntry;
import feature.TagFeature;

//Source : https://www.mkyong.com/java/how-to-read-and-parse-csv-file-in-java/
public class CSVReader {
	static List<Track> trackList = new ArrayList<Track>();	
	//list of tag feature entries
	
	static List<FeatureEntry> tagFeatureList = new ArrayList<FeatureEntry>();

    static String cvsSplitBy = ",";
     
    /* 
     * Find features where the given cleanTagId is dominant. 
     * The featureList contains a list of feature id's (number) to which the given Tag belongs
     * returns a list with the feature numbers
    */ 
	public static List<Integer> findTagFeature( int cleanTagId, double threshold ){
		List<Integer> featureList = new ArrayList<Integer>();

    	//get tag name of the given cleandId
    	String tagName = DBHandler.getTagName(cleanTagId).toLowerCase();
    	
		if( tagFeatureList.size() > 0 ){
	    	for( FeatureEntry tagFeatureEntry : tagFeatureList ){
	    		//skip feature 0
	    		if( tagFeatureEntry.getName().toLowerCase().equals( tagName ) && tagFeatureEntry.getFeature() != 0 ){
	    			if( tagFeatureEntry.getDominance() > threshold ){
	    				featureList.add( tagFeatureEntry.getFeature() );
	    				System.out.println("Feature " + tagFeatureEntry.getFeature()+ " contains this tag with the required minimum dominance. Dominance "+ tagFeatureEntry.getDominance() );
	    			}
	    		}
	    	}
		}else{
			System.out.println("No tag features available");
		}
		return featureList;
	}
    
    
    //add tags to the track of the given featureList
    public static void addTags(List<Integer> featureList){
    	
    	for( Track t : trackList ){
    		int featureNr = t.getFeatureNr();   		
    		
    		if( featureList.contains( featureNr ) ){
    		
        		int realTrackId = t.getTrackId();
        		
        		//System.out.println("Get track tags for track with realId: "+realTrackId);
            	ArrayList<Integer> trackTags = DBHandler.getTrackTags( realTrackId );
            	
            	//tagId is realId --> WRONG change with cleanTagId later
            	for( int realTagId : trackTags ){
            		int cleanTagId = DBHandler.getCleanTagId( realTagId );
            		//add tag with the proper cleanId, second param is the lastFMWeight wich is not important here
            		t.addTag(cleanTagId, 0);
            	}		
    		}

    	} 	
    }
    

    /*
     * Reads the csv file with the song features
     * skipFirstFeature: if true skip feature 0
     * fixTrackIds: if true call protected fixRealIds method
     * return a trackList which is an array with all tracks of all features
     */
    public static List<Track> readTrackListFromCSV( String csvFile, boolean skipFirstFeature, boolean fixTrackIds){
    	List<Track> trackList = new ArrayList<Track>();	

        BufferedReader br = null;
        String line = "";

        try {
            br = new BufferedReader( new FileReader(csvFile) );
            //first line is the header
            line = br.readLine();
            while ((line = br.readLine()) != null) {
            	
                String[] song = line.split(cvsSplitBy);

                int featureNr = Integer.parseInt( song[0] );
                int id = Integer.parseInt( song[1] );
                String name = song[2];
                double dominance = Double.parseDouble( song[3] );
                
                if( featureNr == 0 && skipFirstFeature )continue;
                
                //!!! WRONGid; id is here the same as cleanId -> fix later with fixRealId method
                Track tmpTrack = new Track( id );
                tmpTrack.setFeatureNr( featureNr );
                tmpTrack.setCleanId( id );
                tmpTrack.setName( name );
                tmpTrack.setDominance( dominance );
                                    
        		//add track to trackList
        		trackList.add(tmpTrack);
                //System.out.println("Song [FeatureNr=" + song[0] + ", id=" + song[1] + " , Name=" + song[2] + ", Dominance=" + song[3] + "]");
            }

        } 
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }       
        if( fixTrackIds )fixRealIds(trackList);
       
        return trackList;
    }
    
    
    /*
     * Overrides the realId of the tracks in the trackList which is now wrongly the cleanId. 
     * This duplicate is replaced by the realId of the track. 
     */
    private static void fixRealIds(List<Track> trackList){  
    	for( Track t : trackList ){
    		int realId = DBHandler.getRealTrackId( t.getCleanId() );
        	t.setTrackId( realId );
    	}	   	
    }
    
    
    /*
    * reads the feature csv file of the given path
    * returns a list with all features and their entries
    */ 
    public static List<TagFeature> readTagFeaturesFromCSV( String csvFile, boolean skipFirstFeature){
    	List<TagFeature> tagFeatures = new ArrayList<TagFeature>();
    	
        BufferedReader br = null;
        String line = "";
        try {
            br = new BufferedReader( new FileReader(csvFile) );
            
            //first line is the header
            line = br.readLine();
            
            //feature counter, starts with 1 if feature 0 should be skipped       
            int tmpFeatureNr = skipFirstFeature == true ? 1 : 0;           
            TagFeature currentFeature = new TagFeature( tmpFeatureNr );
            
            while ((line = br.readLine()) != null) {
            	
                String[] tagFeature = line.split(cvsSplitBy);
                int featureNr = Integer.parseInt( tagFeature[0] );
                
                int id = Integer.parseInt( tagFeature[1] );
                String name = tagFeature[2];
                double dominance = Double.parseDouble( tagFeature[3] );
                              
                //skip first feature if given boolen is true
                if( featureNr == 0 && skipFirstFeature )continue;
                                
                //create a tag feature entry from the current csv line
                FeatureEntry tagFeatureEntry = new FeatureEntry(featureNr, id, name, dominance);

                //the tag feature entry belongs to the same feature
                if( tmpFeatureNr == featureNr){
                	currentFeature.addFeatureEntry( tagFeatureEntry );
                }else{ //next feature                	
                    //add current feature to feature list  
                    tagFeatures.add( currentFeature );       
                    
                	currentFeature = new TagFeature( featureNr );
                	currentFeature.addFeatureEntry( tagFeatureEntry );

                    tmpFeatureNr++;
                }     
          }
        } 
        catch (FileNotFoundException e) {
            e.printStackTrace();
        } 
        catch (IOException e) {
            e.printStackTrace();
        } 
        finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }     
        return tagFeatures;
    }
    

}

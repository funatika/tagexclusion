package utility;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.OpenMapRealMatrix;
import org.apache.commons.math3.linear.RealMatrix;
//import org.apache.commons.math3.linear.SingularValueDecomposition;

import Jama.Matrix;

public class Utility {

	
	//returns a RealMatrix
	public static RealMatrix convertJamaToRealMatrix(Matrix m){
		//gets the array from Jama matrix and return a new RealMatrix
		return MatrixUtils.createRealMatrix(  m.getArray() );
	}
	
	//returns a SparseRealMatrix
	public static OpenMapRealMatrix convertJamaToSparseRealMatrix(Matrix m){
		//gets the array from Jama matrix and return a new RealMatrix
		return createSparseMatrix(  m.getArray() );
	}
	
	
	//returns a Jama Matrix
	public static Matrix convertRealMatrixToJamaMatrix(RealMatrix m){
		//gets the array from RealMatrix and return a new Jama matrix
		return new Matrix( m.getData() );
	}
	
	
    public static OpenMapRealMatrix createSparseMatrix(double[][] data) {
        OpenMapRealMatrix matrix = new OpenMapRealMatrix(data.length, data[0].length);
        for (int row = 0; row < data.length; row++) {
            for (int col = 0; col < data[row].length; col++) {
                matrix.setEntry(row, col, data[row][col]);
            }
        }
        return matrix;
    }
	
	
    //save matrix in dense text format
	public static void saveMatrix( Matrix m, String savePath ){
		   if( m == null)return;
		   double [][] tmpArr = m.getArray();
		   
		   if( tmpArr.length == 0 )return;
		   
		   try( PrintWriter pwriter = new PrintWriter( savePath )  ){
			   
			   //first line of the persistent file is for the dimension cols X rows
				pwriter.println(tmpArr[0].length + " "+ tmpArr.length);

				for(int i=0; i < tmpArr.length; i++){		
					
					String line="";
					
					for(int j=0; j < tmpArr[i].length; j++){
						line = line + tmpArr[i][j] + " ";
					}
					pwriter.println(line);
				}	
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
	   }
	
	//save matrix in dense text format to file
	public static void saveMatrixDt( Matrix m, String savePath ){		
		try( PrintWriter pwriter = new PrintWriter( savePath )  ){
			
			double [][] mArr = m.getArray();
			int x = mArr[0].length;
			int y = mArr.length;
			
			//number of all values in the matrix which are not null
			int notNullAll = 0;
			String content = "";
											
			for(int i=0; i < x; i++){	
				String line = "";
				//counts all values which are not 0 from a column
				int notNull= 0;
				
				//iterate over the column
				for(int j=0; j < y; j++){

					//save values in sparse text format only if value is bigger than null
					if( mArr[j][i] > 0 ){
						line = line + j + " " + mArr[j][i] + "\n";
						notNull++;
					}
					notNullAll = notNullAll + notNull;
				}	
				line = notNull + "\n"+line;
				content = content + line;
				System.out.print(line);
			}	
			pwriter.println( x+" "+y+" "+notNullAll );
			pwriter.print(content);		
		}catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
		
		
	   
	   //loads a matrix from file and returns is
	 public static Matrix loadMatrix( String savePath  ){
		   	double [][] tmpArr = null;
		   
			BufferedReader in;
			try {
				in = new BufferedReader(new FileReader( savePath ));		
				
				String line;
				
				//-1 -> tmpArr is not initialized yet
				int counter=-1;
				while((line = in.readLine()) != null){
					//every 2nd element is a empty string
					if(counter==-1){
						String [] values = line.split(" ");

						if(values.length==3){
							int cols = Integer.parseInt( values[0] );
							int rows = Integer.parseInt( values[1] );
							//initialize tmpArray with proper dimension (given in the first line of the persistent file
							tmpArr = new double[rows][cols];
							counter=0;
						}	

					}else{
					    String [] values = line.split(" ");
					    
					    for(int i=0; i < values.length; i++){
					    	//filter empty strings
					    	if( values[i] != " " && values[i] != "" ){
						    	//parse string to int and assign all values from the actual line to the om matrix
					    		tmpArr[counter][i] =  Double.parseDouble( values[i] );
					    	}    	
					    }
					    counter++; 	
					}

				}
				in.close();
			} 
			catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			Matrix m = new Matrix(tmpArr);
			return m;
	   }
	   
	   
		public static Matrix loadMatrixDt( String savePath ){ 
			BufferedReader in;
			double [][] mArr = null; 
			try {
				in = new BufferedReader(new FileReader( savePath ));		
				
				int x, y;
				
				String line;
			    String [] dimension = in.readLine().split(" ");
			    
			    
			    if( dimension.length == 3){
			    	x = Integer.parseInt( dimension[0] );
			    	y = Integer.parseInt( dimension[1] );
			    	
			    	mArr = new double [y][x];
			    }else{
			    	in.close();
			    	return null;
			    }
			   
			    int activeCol = 0;
				while((line = in.readLine()) != null){
					//number of values which are not null from the actual column
					int colValuesNotNull = Integer.parseInt( line  );

					for(int i=0; i < colValuesNotNull; i++){
					    //key value pair. first value is the position of the value, second is the value
				    	if( (line = in.readLine()) != null ){
						    String [] keyValuePair = line.split(" ");
						    
						    if( keyValuePair.length == 2 ){
							    int posValue = Integer.parseInt( keyValuePair[0]  );
							    double value = Double.parseDouble( keyValuePair[1]  );

							    mArr[posValue][activeCol] = value;
						    }
				    	}
				    }
				    activeCol++;
				}
				in.close();
			} 
			catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			return new Matrix(mArr);
		}


}
